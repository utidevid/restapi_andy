<?php

namespace RestApi\RequestCalls;

class Requests
{
    // Content types
    const APPLICATION_JSON_CONTENT_TYPE = "application/json";

    // Request types
    const POST = "POST";
    const GET = "GET";
    const PUT = "PUT";
    const DELETE = "DELETE";

    // Status codes
    const OK_REQUEST_CODE = 200;
    const BAD_REQUEST_CODE = 400;
    const INTERNAL_SERVER_ERROR_REQUEST_CODE = 500;
    const CREATED_REQUEST_CODE = 201;
    const NOT_FOUND_REQUEST_CODE = 404;

    // Status names
    const OK_REQUEST = "OK";
    const BAD_REQUEST_REQUEST = "Bad Request";
    const INTERNAL_SERVER_ERROR_REQUEST = "Internal Server Error";
    const CREATED_REQUEST = "Created";
    const NOT_FOUND_REQUEST = "Not Found";

    /**
     * Get responses array
     *
     * @return array
     */
    static public function getRequests()
    {
        return array(
            self::OK_REQUEST_CODE => self::OK_REQUEST,
            self::BAD_REQUEST_CODE => self::BAD_REQUEST_REQUEST,
            self::INTERNAL_SERVER_ERROR_REQUEST_CODE => self::INTERNAL_SERVER_ERROR_REQUEST,
            self::CREATED_REQUEST_CODE => self::CREATED_REQUEST,
            self::NOT_FOUND_REQUEST_CODE => self::NOT_FOUND_REQUEST,
        );
    }
}