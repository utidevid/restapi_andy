<?php

namespace RestApi\Controllers;

use RestApi\Db\SqlDbConn;
use RestApi\Models\Address;
use RestApi\Msg\MsgGenerator;
use RestApi\Msg\MsgOutputter;
use RestApi\RequestCalls\Request;
use RestApi\RequestCalls\Requests;

class AddressesController
{
    /**
     * @var Address
     */
    private $address;

    /**
     * @var Request
     */
    private $request;

    /**
     * The default action of the controller
     *
     * @param int $id - address ID
     * @return string
     */
    public function indexAction($id = null)
    {
        // Initialize requests
        $this->request = new Request(new MsgOutputter(), new MsgGenerator());

        // Set supported request methods
        $this->request->setSupportedRequestMethods(
            array(Requests::POST, Requests::GET, Requests::DELETE, Requests::PUT)
        );

        // Set supported content types
        $this->request->setSupportedContentTypes(
            array(Requests::APPLICATION_JSON_CONTENT_TYPE)
        );

        // Check if passed requested method is supported
        if (!$this->request->isRequestMethodSupported()) {
            echo $this->request->respondJson(Requests::BAD_REQUEST_CODE);
            die;
        }

        $this->address = new Address(SqlDbConn::connect());

        switch ($this->request->getRequestMethod()) {
            case Requests::POST:
                echo $this->createAddress($id);
                break;

            case Requests::GET:
                echo $this->getAddresses($id);
                break;

            case Requests::PUT:
                echo $this->updateAddress($id);
                break;

            case Requests::DELETE:
                echo $this->deleteAddress($id);
                break;
        }
    }

    /**
     * Create address
     *
     * @param $id
     * @return string
     */
    private function createAddress($id)
    {
        // Abort request if ID is present or content type is not supported
        if ($id != null || !$this->request->isContentTypeSupported())
            return $this->request->respondJson(Requests::BAD_REQUEST_CODE);

        // Get json data
        $data = $this->request->getJsonData();

        // Check if json is invalid
        if ($data == null)
            return $this->request->respondJson(Requests::BAD_REQUEST_CODE);

        // Set data to the address model
        $this->address->setData($data);

        // Check if validation succeeds
        if ($this->address->validate()) {
            // Check if data persists
            if ($this->address->persist())
                return $this->request->respondJson(Requests::CREATED_REQUEST_CODE, 'Submitted data is created');

            // Data could not persist
            return $this->request->respondJson(Requests::INTERNAL_SERVER_ERROR_REQUEST_CODE);
        }

        // Validation fails
        return $this->request->respondJson(Requests::BAD_REQUEST_CODE, 'Submitted data is invalid',
            $this->address->getValidationErrors());
    }

    /**
     * Update address
     *
     * @param $id
     * @return string
     */
    private function updateAddress($id)
    {
        // Abort request if ID is not present or content type is not supported
        if ($id == null || !$this->request->isContentTypeSupported())
            return $this->request->respondJson(Requests::BAD_REQUEST_CODE);

        // Retrieved address by ID
        $address = $this->address->findById($id);

        // If address does not exists, throw not found
        if ($address == null)
            return $this->request->respondJson(Requests::NOT_FOUND_REQUEST_CODE, "No address with such ID is found.");

        // Get json data
        $data = $this->request->getJsonData();

        // Check if json is invalid
        if ($data == null)
            return $this->request->respondJson(Requests::BAD_REQUEST_CODE);

        // Set data to the address model
        $this->address->setData($data);

        // If validation succeeds
        if ($this->address->validate()) {
            // Check if data persists
            if ($this->address->persist())
                return $this->request->respondJson(Requests::OK_REQUEST_CODE, 'Submitted data is updated');

            // Data could not persist
            return $this->request->respondJson(Requests::INTERNAL_SERVER_ERROR_REQUEST_CODE);
        }

        // Validation fails
        return $this->request->respondJson(Requests::BAD_REQUEST_CODE, 'Submitted data is invalid',
            $this->address->getValidationErrors());
    }

    /**
     * Delete address
     *
     * @param $id
     * @return string
     */
    private function deleteAddress($id)
    {
        // Abort request if ID is not present
        if ($id == null)
            return $this->request->respondJson(Requests::BAD_REQUEST_CODE);

        // Retrieved address by ID
        $address = $this->address->findById($id);

        // If address does not exists, throw not found
        if ($address == null)
            return $this->request->respondJson(Requests::NOT_FOUND_REQUEST_CODE, "No address with such ID is found.");

        // Attempt to delete an address
        if ($this->address->delete())
            return $this->request->respondJson(Requests::OK_REQUEST_CODE, 'Entry is deleted.');

        // Deletion fails
        return $this->request->respondJson(Requests::INTERNAL_SERVER_ERROR_REQUEST_CODE);
    }

    /**
     * Get addresses
     *
     * @param $id
     * @return string
     */
    private function getAddresses($id)
    {
        // If ID is present, get address by ID
        if ($id == null)
            return $this->getAddressesJson();

        // Get all addresses
        return $this->getAddressJson($id);
    }

    /**
     * Get address by ID
     *
     * @param $id
     * @return string
     */
    private function getAddressJson($id)
    {
        // Retrieved address
        $address = $this->address->findById($id);

        // If address does not exist, throw not found
        if ($address == null)
            return $this->request->respondJson(Requests::NOT_FOUND_REQUEST_CODE, "No address with such ID is found.");

        // Successful headers
        $this->request->respondHeadersJson();

        // Return addresses
        return json_encode($address);
    }

    /**
     * Get addresses
     *
     * @return string
     */
    private function getAddressesJson()
    {
        // Successful headers
        $this->request->respondHeadersJson();

        return json_encode($this->address->findAll());
    }
}