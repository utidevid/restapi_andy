<?php

namespace RestApi\Controllers;

class DefaultController
{
    /**
     * The default action of the controller
     */
    public function indexAction()
    {
        echo "Hello World, RestAPI! Go to <a href=\"/addresses\">addressses</a>";
    }
}