<?php

namespace RestApi;

interface FrontControllerInterface
{
    const CONTROLLER_OPTION = "controller";
    const ACTION_OPTION = "action";
    const PARAMS_OPTION = "params";

    /**
     * Set controller
     *
     * @param $controller
     * @return mixed
     */
    public function setController($controller);

    /**
     * Set action of the controller
     *
     * @param $action
     * @return mixed
     */
    public function setAction($action);

    /**
     * Set parameters of the action
     *
     * @param array $params
     * @return mixed
     */
    public function setParams(array $params);

    /**
     * Run the appropriate action in a controller along with the specified parameters
     *
     * @return mixed
     */
    public function run();
}