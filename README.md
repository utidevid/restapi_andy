# README #

The project is a simple, restful web application that currently supports standard crud operations on physical addresses. The data are being sent and retrieved in JSON format.

### Requirements ###

* Make sure the latest version of PHP is installed
* If you use Google Chrome, make sure it has a rest client installed. There are extensions available as Postman and Advanced Rest Client for Google Chrome. Other browsers should have appropriate extensions / plugins available, too

### Setup ###

* Clone the project to an executable directory of your php engine
* Create a table in your database from the dump.sql file that can be found in the project root folder
* Edit config file of the project according to your local settings. The config file can be found in the config folder of the project

### Usage ###

* Open a rest client of your choice and be sure to select the application/json content type
* To create the first record, submit the POST request http://localhost/addresses and enter the raw JSON content of your choice based on the following pattern:
{"LABEL": "John Doe","STREET": "Robin st.","HOUSENUMBER": "22b","POSTALCODE": "LU99OO","CITY": "London","COUNTRY": "United Kingdom"}
Note that all fields are mandatory. The request should return 201 Created on success, 400 Bad Request or 500 Internal Server Error.
* To retrieve the just created records, submit the GET request http://localhost/addresses/{ID}. The request should return 200 OK or 404 Not Found.
* To update the records, submit the PUT request http://localhost/addresses/{ID} and pass the raw JSON content, like: {"LABEL": "Jane Doe","STREET": "Robin Twice st."}. The request should return 200 OK, 404 Not Found, 400 Bad Request or 500 Internal Server Error.
Note that mentioning all of the JSON entries is not important. The script will update those which are mentioned in the raw JSON content. 
* To delete the records, submit the DELETE request http://localhost/addresses/{ID}. The request should return 200 OK, 404 Not Found, 500 Internal Server Errror, 400 Bad Request.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Should you have any questions or seek further clarifications, please feel free to contact me at andy.zaporozhets@gmail.com