<?php

namespace RestApi\Models;

use RestApi\Db\DbConnInterface;

abstract class Model
{
    // Table data
    protected $id;

    // Entry data
    protected $data = array();

    // Validation error stack
    protected $validationErrors = array();

    // Database connection
    protected $dbConn;

    /**
     * @param DbConnInterface $dbConn - Initiate connection to the database
     */
    public function __construct(DbConnInterface $dbConn)
    {
        $this->dbConn = $dbConn;
        $this->dbConn->setTable($this->getTableName());
        $this->dbConn->setIdColumn($this->getIdColumnName());
    }

    /**
     * Set entry data
     *
     * @param array $data
     */
    public function setData(array $data = array())
    {
        // Loop through the collected data
        foreach ($data as $key => $val) {

            // If data value is appropriate, add it to the data array
            if (in_array(strval($key), $this->getAvailableColumnNames()))
                $this->data[$key] = $val;
        }
    }

    /**
     * Get table ID column name
     *
     * @return string
     */
    abstract public function getIdColumnName();

    /**
     * Get table name
     *
     * @return string
     */
    abstract public function getTableName();

    /**
     * Get available columns of the model apart from ID
     *
     * @return array
     */
    abstract protected function getAvailableColumnNames();

    /**
     * Add validation error
     *
     * @param $column
     * @param $error
     */
    protected function addValidationError($column, $error)
    {
        $this->validationErrors[$column][] = $error;
    }

    /**
     * Create validation rules
     */
    abstract public function validationRules();

    /**
     * Check if data is valid
     * @return bool
     */
    public function validate()
    {
        // Collect errors by looping through the validation rules
        $this->validationRules();

        // Check if submitted data count is valid
        if (count($this->data) < count($this->getAvailableColumnNames()))
            return false;

        // If no validation errors present, the validation passes successfully
        return count($this->validationErrors) == 0;
    }


    /**
     * Persist data to the database
     *
     * @return int - a number of affected rows
     */
    public function persist()
    {
        // Execute an update if an ID is present in data
        if ($this->id != null) {
            // Add ID to the params
            $this->data[$this->getIdColumnName()] = $this->id;

            // If ID is present, run an update statement and check if one record is updated
            return $this->dbConn->update($this->data);
        }
        // Otherwise, execute an insert of no ID is present
        else
            // Run an insert statement and check if one records is added
            return $this->dbConn->insert($this->data);
    }

    /**
     * Delete address by ID
     *
     * @return int
     */
    public function delete()
    {
        return $this->dbConn->delete(array($this->getIdColumnName() => $this->id));
    }

    /**
     * Find all records
     *
     * @return array
     */
    public function findAll()
    {
        return $this->dbConn->select();
    }

    /**
     * Find record by ID
     *
     * @param $id
     * @return array|null
     */
    public function findById($id)
    {
        $data = $this->dbConn->select(array($this->getIdColumnName() => $id));

        // Check if data exists in the array
        if (isset($data[0])) {
            // Set address data to the data array
            $this->setData($data[0]);

            // Set id when the address is found
            $this->id = $id;

            // Return the current address
            return $data;
        }

        return null;
    }

    /**
     * Get validation errors
     *
     * @return array
     */
    public function getValidationErrors()
    {
        return $this->validationErrors;
    }

    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}