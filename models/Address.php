<?php

namespace RestApi\Models;

class Address extends Model
{
    // Table columns
    const ID_COLUMN = "ADDRESSID";
    const LABEL_COLUMN = "LABEL";
    const STREET_COLUMN = "STREET";
    const HOUSENUMBER_COLUMN = "HOUSENUMBER";
    const POSTALCODE_COLUMN = "POSTALCODE";
    const CITY_COLUMN = "CITY";
    const COUNTRY_COLUMN = "COUNTRY";

    // Table name the model deals with
    const TABLE_NAME = "ADDRESS";

    /**
     * Get table ID column name
     *
     * @return string
     */
    public function getIdColumnName()
    {
        return self::ID_COLUMN;
    }

    /**
     * Get table name
     *
     * @return string
     */
    public function getTableName()
    {
        return self::TABLE_NAME;
    }

    /**
     * Get available columns of the model apart from ID
     *
     * @return array
     */
    protected function getAvailableColumnNames()
    {
        return array(self::LABEL_COLUMN, self::STREET_COLUMN, self::HOUSENUMBER_COLUMN, self::POSTALCODE_COLUMN,
            self::CITY_COLUMN, self::COUNTRY_COLUMN);
    }

    /**
     * Validate entry data
     */
    public function validationRules()
    {
        // Loop through the data and validate it
        foreach ($this->data as $key => $value) {
            // Check if value is not empty
            if (empty($value))
                $this->addValidationError($key, "Should not be empty");

            // Check if house number length is not greater than 10
            if ($key == self::HOUSENUMBER_COLUMN && strlen($value) > 10)
                $this->addValidationError($key, "Should not exceed 10 characters");

            // Check if postcode length is not greater than 6
            if ($key == self::POSTALCODE_COLUMN && strlen($value) > 6)
                $this->addValidationError($key, "Should not exceed 6 characters");

            // Check if any other value is not greater than 100
            if (!in_array($key, array(self::HOUSENUMBER_COLUMN, self::POSTALCODE_COLUMN)) && strlen($value) > 100)
                $this->addValidationError($key, "Should not exceed 100 characters");
        }
    }
}