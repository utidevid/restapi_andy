<?php

namespace RestApi;

use InvalidArgumentException;

class FrontController implements FrontControllerInterface
{
    // Default controller suffix
    const CONTROLLER_SUFFIX = "Controller";
    // Default action suffix
    const ACTION_SUFFIX = "Action";

    // Default controller name
    const DEFAULT_CONTROLLER = "Default";
    // Default action name
    const DEFAULT_ACTION = "index" . self::ACTION_SUFFIX;

    protected $controller = self::DEFAULT_CONTROLLER;
    protected $action = self::DEFAULT_ACTION;
    protected $params = array();

    // The url array to store controller, action and their parameters
    private $url = array();

    /**
     * Assemble a new URL or parse the request URL
     *
     * @param array $options - parse the request URL
     */
    public function __construct(array $options = array())
    {
        if (count($options) == 0)
            $this->parseUri();
        else {
            if (isset($options[FrontControllerInterface::CONTROLLER_OPTION]))
                $this->setController($options[FrontControllerInterface::CONTROLLER_OPTION]);

            if (isset($options[FrontControllerInterface::ACTION_OPTION]))
                $this->setAction($options[FrontControllerInterface::ACTION_OPTION]);

            if (isset($options[FrontControllerInterface::PARAMS_OPTION]))
                $this->setParams($options[FrontControllerInterface::PARAMS_OPTION]);
        }
    }

    /**
     * Parse URI and determine controller, action and params part of the URI
     */
    protected function parseUri()
    {
        // Get request URL without the forward slash "/"
        $path = trim(parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH), "/");

        // The URL consist of controller, action and an array of parameters
        $this->url = explode("/", $path, 3);

        // Check if controller is present
        if (isset($this->url[0]))
            $this->setController('\\RestApi\\Controllers\\' .
                (empty($this->url[0]) ? self::DEFAULT_CONTROLLER : $this->url[0]));

        // Check if action is present
        if (isset($this->url[1]))
            $this->setAction($this->url[1]);

        // Check if parameters is present
        if (isset($this->url[2]))
            $this->setParams(explode("/", $this->url[2]));
    }

    /**
     * Set controller
     *
     * @param $controller
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function setController($controller)
    {
        // Compose a controller name to match the required syntax
        $controller = ucfirst(strtolower($controller)) . self::CONTROLLER_SUFFIX;

        // If a controller class is not found, throw an exception
        if (!class_exists($controller))
            throw new InvalidArgumentException("The controller {$controller} has not been defined.");

        // Set the controller to the class
        $this->controller = $controller;

        return $this;
    }

    /**
     * Set action of the controller
     *
     * @param $action
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function setAction($action)
    {
        // Compose an action name to match the required syntax
        $method = strtolower($action) . self::ACTION_SUFFIX;

        // If a custom method in the controller class does not exist
        if (!method_exists(new $this->controller, $method)) {
            // Check if the default method exists to call the default action
            if (method_exists(new $this->controller, self::DEFAULT_ACTION)) {
                // Move parameter if exists, from the action part to the params of the URL
                $this->moveParamFromActionToParams($action);

                // Set action part of the URL to be equaled to the default action
                $method = self::DEFAULT_ACTION;
            }
            // Otherwise, throw an exception
            else
                throw new InvalidArgumentException("The controller action {$action} has been not defined.");
        }

        // Set the method as an action of the class
        $this->action = $method;

        return $this;
    }

    /**
     * Set parameters of the action
     *
     * @param array $params
     * @return mixed
     */
    public function setParams(array $params)
    {
        $this->params = $params;

        return $this;
    }

    /**
     * Run the appropriate action in a controller along with the specified parameters
     *
     * @return mixed
     */
    public function run()
    {
        call_user_func_array(
            array(new $this->controller, $this->action),
            $this->params
        );
    }

    /**
     * Move a parameter from the action part of the URL to the params
     * @param $action
     */
    private function moveParamFromActionToParams($action)
    {
        // Check if params part is not empty
        if (!empty($this->url[2])) {
            // Merge parameter taken from the action part with the rest of parameters that may appear in the params
            $this->url[2] = $action . '/' . $this->url[2];
        }
        // Otherwise, set a parameter taken from action to the params part
        else
            $this->url[2] = $action;
    }
}