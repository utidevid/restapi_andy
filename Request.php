<?php

namespace RestApi\RequestCalls;

use RestApi\Msg\MsgGeneratorInterface;
use RestApi\Msg\MsgOutputterInterface;

class Request
{
    /**
     * Request method
     *
     * @var string
     */
    protected $requestMethod;

    /**
     * Request content type
     *
     * @var array
     */
    protected $requestContentTypes = array();

    /**
     * Array of supported request methods
     *
     * @var array
     */
    protected $supportedRequestMethods = array();

    /**
     * Array of supported content types
     *
     * @var array
     */
    protected $supportedContentTypes = array();

    /**
     * @var MsgOutputterInterface
     */
    protected $msgOutputter;

    /**
     * @var MsgGeneratorInterface
     */
    protected $msgGenerator;

    /**
     * Initialize request
     *
     * @param MsgOutputterInterface $msgOutputter - set message outputter class
     * @param MsgGeneratorInterface $msgGenerator - set message generator class
     */
    public function __construct(MsgOutputterInterface $msgOutputter, MsgGeneratorInterface $msgGenerator)
    {
        $this->requestMethod = $_SERVER['REQUEST_METHOD'];
        $this->requestContentTypes = isset($_SERVER["CONTENT_TYPE"]) ?
            array_map('trim', explode(',', $_SERVER["CONTENT_TYPE"])) : null;
        $this->msgOutputter = $msgOutputter;
        $this->msgGenerator = $msgGenerator;
    }

    /**
     * Get json data and return it as an assoc array
     *
     * @return array|null
     */
    public function getJsonData()
    {
        // Get json data
        $jsonData = file_get_contents('php://input');

        // Turn json data into an assoc array
        return json_decode($jsonData, true);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        return $_POST;
    }

    /**
     * Respond with headers
     *
     * @param int $requestStatusCode
     * @param $contentTypeCallback
     */
    public function respondHeaders($requestStatusCode = Requests::OK_REQUEST_CODE, $contentTypeCallback)
    {
        // Response content type
        $contentTypeCallback();

        // Response status
        header("HTTP/1.0 {$requestStatusCode} " . Requests::getRequests()[$requestStatusCode]);
    }

    /**
     * Respond with headers in JSON format
     *
     * @param int $requestStatusCode
     */
    public function respondHeadersJson($requestStatusCode = Requests::OK_REQUEST_CODE)
    {
        $this->respondHeaders($requestStatusCode, function() {
            header('Content-Type: application/json');
        });
    }

    /**
     * Respond to a client
     *
     * @param int $requestStatusCode
     * @param string $msg
     * @param array $errors
     * @param $contentTypeCallback
     *
     * @return MsgOutputterInterface
     */
    public function respond($requestStatusCode = Requests::OK_REQUEST_CODE, $msg = null, array $errors = array(), $contentTypeCallback)
    {
        // Respond headers
        $this->respondHeaders($requestStatusCode, $contentTypeCallback);

        // If message is empty, set it to request status name
        if (empty($msg))
            $msg = Requests::getRequests()[$requestStatusCode];

        // Custom error message
        return $this->msgOutputter
            ->setMessage($this->msgGenerator->createMessage($requestStatusCode, $msg, $errors));
    }

    /**
     * Respond to a client in JSON format
     *
     * @param int $requestStatusCode
     * @param $msg
     * @param array $errors
     * @return string
     */
    public function respondJson($requestStatusCode = Requests::OK_REQUEST_CODE, $msg = null, array $errors = array())
    {
        return $this->respond($requestStatusCode, $msg, $errors, function() {
            header('Content-Type: application/json');
        })->json();
    }

    /**
     * Set supported request methods
     *
     * @param array $requestMethods
     */
    public function setSupportedRequestMethods(array $requestMethods = array())
    {
        $this->supportedRequestMethods = $requestMethods;
    }

    /**
     * Check if request method is supported
     *
     * @return bool
     */
    public function isRequestMethodSupported()
    {
        return in_array($this->requestMethod, $this->supportedRequestMethods);
    }

    /**
     * Get request method
     *
     * @return string
     */
    public function getRequestMethod()
    {
        return $this->requestMethod;
    }

    /**
     * Set supported content types
     *
     * @param array $contentTypes
     */
    public function setSupportedContentTypes(array $contentTypes = array())
    {
        $this->supportedContentTypes = $contentTypes;
    }

    /**
     * Check if content type is supported
     *
     * @return bool
     */
    public function isContentTypeSupported()
    {
        foreach ($this->requestContentTypes as $contentType) {
            foreach ($this->supportedContentTypes as $supportedContentType) {
                if ($contentType == $supportedContentType)
                    return true;
            }
        }
    }

    /**
     * Get content type
     *
     * @return array
     */
    public function getContentTypes()
    {
        return $this->requestContentTypes;
    }
}