<?php

namespace RestApi\Db;

interface DbConnInterface
{
    /**
     * Set table name
     *
     * @param $table - table name
     * @return DbConnInterface
     */
    public function setTable($table);

    /**
     * Set ID column name
     *
     * @param $column
     * @return DbConnInterface
     */
    public function setIdColumn($column);

    /**
     * Create a query
     *
     * @param $query
     * @return DbConnInterface
     */
    public function query($query);

    /**
     * Select a record
     *
     * @param array $params
     * @return array - retrieves data as an assoc array
     */
    public function select(array $params = array());

    /**
     * Update data
     *
     * @param array $params
     * @return int - number of rows affected
     */
    public function update(array $params = array());

    /**
     * Insert data
     *
     * @param array $params
     * @return int - number of rows affected
     */
    public function insert(array $params = array());

    /**
     * Delete data
     *
     * @param array $params
     * @return int - number of rows affected
     */
    public function delete(array $params = array());

    /**
     * Fetch data and fetch data as an assoc array
     *
     * @param array $params
     * @return array
     */
    public function fetch(array $params = array());

    /**
     * Execute an update, delete or insert queries
     *
     * @param array $params
     * @return int - rows affected
     */
    public function execute(array $params = array());

    /**
     * Close the connection
     * @return mixed
     */
    public function close();

    /**
     * Connect to the database
     * @return mixed
     */
    static public function connect();
}