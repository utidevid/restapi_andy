<?php

namespace RestApi\Db;

use PDO;

class SqlDbConn implements DbConnInterface
{
    private $db;
    private $pdoStatement;
    private $table;
    private $idKey;


    public function __construct($host, $db, $user, $password = '')
    {
        $this->db = new \PDO("mysql:host={$host};dbname={$db}", $user, $password);
    }

    /**
     * Create a query
     *
     * @param $query
     * @return DbConnInterface
     */
    public function query($query)
    {
        // Prepare a statement
        $this->pdoStatement = $this->db->prepare($query);

        return $this;
    }

    /**
     * Execute a query and fetch data as an assoc array
     *
     * @param array $params
     * @return array
     */
    public function fetch(array $params = array())
    {
        // Execute the statement
        $this->pdoStatement->execute($params);

        // Fetch data
        return $this->pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Execute an update, delete or insert queries
     *
     * @param array $params
     * @return int - rows affected
     */
    public function execute(array $params = array())
    {
        // Execute the statement
        $this->pdoStatement->execute($params);

        // Return a number of rows affected
        return $this->pdoStatement->rowCount();
    }

    /**
     * Select a record
     *
     * @param array $params
     * @return array - retrieves data as an assoc array
     */
    public function select(array $params = array())
    {
        // Create a basic select query
        $query = "select * from {$this->table}";

        // If ID is present in the params
        if (isset($params[$this->idKey]))
            // Create a query with a where statement
            $query = "select * from {$this->table} where {$this->idKey}=:{$this->idKey}";

        // Issue an appropriate query
        $this->query($query);

        // Convert params for the query
        $this->convertParams($params);

        // Execute a select query
        return $this->fetch($params);
    }

    /**
     * Update data
     *
     * @param array $params
     * @return int - number of rows affected
     */
    public function update(array $params = array())
    {
        // Create query columns to successfully run a mysql query
        $queryColumnsValues = implode(',', array_map(
            function($val) {
                return "{$val}=:{$val}";
            },
            array_keys($params)
        ));

        // Convert params for the query
        $this->convertParams($params);

        // Execute an update query
        return $this->query("update {$this->table} set {$queryColumnsValues} where {$this->idKey}=:{$this->idKey}")->execute($params);
    }

    /**
     * Insert data
     *
     * @param array $params
     * @return int - number of rows affected
     */
    public function insert(array $params = array())
    {
        // Create a string of fields that should be updated
        $queryColumns = implode(',', array_keys($params));

        // Create a string of values that correspond their fields
        $queryValues = implode(',', array_map(
            function($val){
                return ':' . $val;
            },
            array_keys($params)
        ));

        // Convert params for the query
        $this->convertParams($params);

        // Execute an insert query
        return $this->query("insert into {$this->table}({$queryColumns}) values({$queryValues})")->execute($params);
    }

    /**
     * Delete data
     *
     * @param array $params
     * @return int - number of rows affected
     */
    public function delete(array $params = array())
    {
        // Convert params for the query
        $this->convertParams($params);

        // Execute a delete query
        return $this->query("delete from {$this->table} where {$this->idKey}=:{$this->idKey}")->execute($params);
    }

    /**
     * Set table name
     *
     * @param $table - table name
     * @return DbConnInterface
     */
    public function setTable($table)
    {
        $this->table = $table;

        return $this;
    }

    /**
     * Set ID column name
     *
     * @param $column
     * @return DbConnInterface
     */
    public function setIdColumn($column)
    {
        $this->idKey = $column;

        return $this;
    }

    /**
     * Close the connection
     * @return mixed
     */
    public function close()
    {
        $this->db = null;
    }

    /**
     * Connect to the database
     * @return SqlDbConn
     */
    static public function connect()
    {
        $config = include 'config/config.php';

        return new SqlDbConn(
            $config['mysql']['host'],
            $config['mysql']['db'],
            $config['mysql']['user'],
            $config['mysql']['password']
        );
    }

    /**
     * Convert params to match query requirements
     *
     * @param array $params
     */
    protected function convertParams(array &$params = array())
    {
        $convertedParams = array();

        // Loop through the data to create params
        foreach ($params as $key => $value) {
            $convertedParams[':' . $key] = $value;
        }

        $params = $convertedParams;
    }
}