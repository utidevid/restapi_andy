<?php

namespace RestApi\Msg;

class MsgGenerator implements MsgGeneratorInterface
{
    protected $statusCode;
    protected $msg;
    protected $errors = array();

    /**
     * Create message
     *
     * @param $statusCode
     * @param $msg
     * @param array $errors
     *
     * @return MsgGeneratorInterface
     */
    public function createMessage($statusCode, $msg, array $errors = array())
    {
        $this->statusCode = $statusCode;
        $this->msg = $msg;
        $this->errors = $errors;

        return $this;
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function getMsg()
    {
        return $this->msg;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Get array of message data
     *
     * @return array
     */
    public function getMessage()
    {
        $msg = array(
            'code' => $this->statusCode,
            'message' => $this->msg
        );

        if (count($this->errors) > 0)
            $msg['errors'] = $this->errors;

        return $msg;
    }
}