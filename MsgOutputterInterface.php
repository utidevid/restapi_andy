<?php

namespace RestApi\Msg;

interface MsgOutputterInterface
{
    /**
     * Set message
     *
     * @param MsgGeneratorInterface $msg
     *
     * @return MsgOutputterInterface
     */
    public function setMessage(MsgGeneratorInterface $msg);

    /**
     * Output message in JSON format
     *
     * @return string
     */
    public function json();
}