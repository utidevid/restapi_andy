<?php

namespace RestApi\Msg;

class MsgOutputter implements MsgOutputterInterface
{
    protected $msg;

    /**
     * Output message in JSON format
     *
     * @return string
     */
    public function json()
    {

        return json_encode($this->msg->getMessage());
    }

    /**
     * Set message
     *
     * @param MsgGeneratorInterface $msg
     *
     * @return MsgOutputterInterface
     */
    public function setMessage(MsgGeneratorInterface $msg)
    {
        $this->msg = $msg;

        return $this;
    }
}