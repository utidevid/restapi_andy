<?php

namespace RestApi\Msg;

interface MsgGeneratorInterface
{
    /**
     * Get array of message data
     *
     * @return array
     */
    public function getMessage();

    /**
     * Create message
     *
     * @param $statusCode
     * @param $msg
     * @param array $errors
     *
     * @return MsgGeneratorInterface
     */
    public function createMessage($statusCode, $msg, array $errors = array());
}