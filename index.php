<?php

namespace RestApi;

// Autoloader to be added
require 'MsgOutputterInterface.php';
require 'MsgGeneratorInterface.php';
require 'MsgGenerator.php';
require 'MsgOutputter.php';
require 'config/config.php';
require 'db/DbConnInterface.php';
require 'db/SqlDbConn.php';
require 'Requests.php';
require 'Request.php';
require 'models/Model.php';
require 'models/Address.php';
require 'controllers/AddressesController.php';
require 'controllers/DefaultController.php';
require 'FrontControllerInterface.php';
require 'FrontController.php';

$frontController = new FrontController();
$frontController->run();